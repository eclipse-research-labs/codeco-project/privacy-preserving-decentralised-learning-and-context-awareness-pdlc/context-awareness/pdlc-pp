# Copyright (c) 2023 fortiss GmbH
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Dalal Ali - Author

import csv
import time
import os
from datetime import datetime


#input_csv = 'New interfaces/data_CA.csv'
input_csv = '/data/CA/data_CA.csv'
def get_metrics():
    # Wait until the directory and file exist
    while not os.path.exists(input_csv):  # or not os.path.isdir('/data/CA'):
        print("Waiting for the directory and input CSV file to exist...")
        time.sleep(0.5)

    # Create a dictionary to store the latest timestamp
    latest_timestamp = None

    # List to store all rows
    all_rows = []

    # Read the CSV file and find the latest timestamp
    with open(input_csv, 'r', encoding='utf-8-sig') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=',')
        for row in reader:
            timestamp = datetime.strptime(row['timestamp'], '%Y-%m-%d %H:%M:%S')
            
            # Update the latest timestamp
            if latest_timestamp is None or timestamp > latest_timestamp:
                latest_timestamp = timestamp

            # Store the row in the list
            all_rows.append(row)

    # Iterate over the stored rows to extract data for nodes with the latest timestamp
    latest_rows = []

    for row in all_rows:
        timestamp = datetime.strptime(row['timestamp'], '%Y-%m-%d %H:%M:%S')
        
        # Check if the timestamp matches the latest timestamp
        if timestamp == latest_timestamp:
            latest_rows.append(row)

    return latest_rows

# Print or use the data in the 'cluster' list as needed
#trial= get_metrics()
#for node in trial:
#    print(f"Node Name: {node}")

