<!--
  ~ Copyright (c) 2023 fortiss GmbH
  ~ 
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~ 
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~ 
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~ 
  ~ SPDX-License-Identifier: Apache-2.0
  ~ 
  ~ Contributors:
  ~     Dalal Ali - Author
-->

# 1. HE-CODECO PDLC sub-component PDLC-CA

PDLC-CA is a sub-component of the HE-CODECO PDLC that focuses on generating an aggregated cost perspective for a cluster or multi-cluster environment.

The repository consists of two branches:
 
**1. main:**

- Contains code for the CODECO framework.
- Enables PDLC-CA to interact with different components within the framework.
- Receives normalized input metrics from PDLC-DP.
- Provides cost output to PDLC-DP, contributing to the scheduling process within the CODECO Scheduler components SWM.



**2. PDLC-CA-Standalone Branch:**

- Provides a framework for using PDLC-CA subcomponents independently.
- Produces a Custom Resource Definition (CRD) with costs for each node based on its performance profile.
- Flexible and extendable to include more metrics.
- Requires CR values from the synthetic data generator as input. You can find the code for the generator [here](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/data-generators-and-datasets/synthetic-data-generator), and it can be adjusted as well, to change the used Prometheus queries to suit your needs in terms of the mtrics to be fetched. 
- Outputs costs as a CR, which can be used by the Kubernetes scheduler to inform scheduling decisions.



The current version of PDLC-CA is focused on a single-cluster operation. It relies on input provided by other CODECO components (e.g., computantional parameters such as CPU, memory; network parameters such as bandwidth, latency, link energy; data metrics such as data compliance, data size).These metrics are gathered and normialzed first by PDLC-DP, and then made available as a CSV file in the PDLC shared volume in the path: /data/CA/data_CA.csv'. The received parameters are read by the _Extractor.py_ file and then used in the _Cost_Estimator.py_ as an aggregation to serve a selected performance target, e.g., optimization of the Kubernetes infrastructure in terms of greeness or resilience. The performance target is provided by the user setting up an application deployment, via the CODECO Application Model. 

Currently, PDLC-CA relies on simple heuristics to rank existing nodes based on computed costs based on the selected target profile, and considering a combination of available data.

The output of PDLC-CA  is made available in the form of a CSV file in a subdirectory of the shared volume between the subcomponents of PDLC in the following path: /data/RL/metrics.csv. The output is used by the to the decentralised learning sub-component of PDLC, PDLC-RL, and also to other components of CODECO, such as SWM.

PDLC-CA performs a combination of the received data based of pre-configured heuristics that aim at providing a measure of performance, for a specific performance target, e.g., greeness.

PDLC-CA-Integration provides examples for two specific target profiles: **greeness** and **resilience**. Other specific targets are under development.

![PDLC-CA](./Picture2.png)


Currently you can test two specific target profiles:

**Greenness Profile**
The purpose is to select nodes that can contribute to achieve an infrastructure (for an application group) that has the least energy expenditure possible.

- Nodes are weighted for energy consumption based on NodeEnergy, Sum LinkEnergy, and not just on the node energy expenditure.
- Prioritizes nodes with lower combined greenness costs (network, computational, data).
- TODO: If multiple paths exist, prioritizes paths with a lower combined cost.

We provide a simple computation of greenness in this early release, for the purpose of experimenting.

 _greenness_cost(n)=node_energy(n)* node_net_energy(n)_, where _n_ is a suitable node and node_net_energy(n) (node network energy) is the sum of the energy consumed by the node links.

The parameters for the node are provided via ACM (prometheus); the links total energy will be provided by the CODECO component NetMA.


**Resilience Profile**

Focuses on the selection of nodes that will maximize the resilience of the infrastructure. 

- Nodes are weighted for their contribution to resilience of the selected infrastructure
- Prioritizes nodes with lower combined resilience costs (network, computational, data)
- @TODO: If multiple paths exist, prioritizes paths with a higher combined cost
- @TODO: May later be combined with feedback from PDLC-DL

_resilience_cost(n)=node_net_failure(n)*sum_link_failure(n)*1/node_degree_(n)

node_failure is a parameter provided by ACM; node_degree and sum_link_failure are parameters expected to be provided by the CODECO NetMA component.

# 2. Pre-requisites to run PDLC-CA


- A cluster needs to be set up. In our case, we provide how-tos that can be used with KinD or minikube.
- The CODECO data generator needs to be installed and run on the cluster to generate a data.csv (input) with the correct format.


# 3. Architectural Design

**Given:**

- CODECO ApplicationModel request (YAML file), comprising application requirements defined during the application setup. 

- Available infrastructure (nodes in a cluster), provided by K8s.

- CODECO CRDs (NetMA, MDM, ACM), holding information about the Kubernetes infrastructure, from a computational, network, and data perspective.

PDLC-CA produces a combined node cost for nodes within the cluster according to their deviation from the chosen performance profile. 


# 4. How to Run

## 4.1 Set up a Kubernetes cluster

Ensure that you have a K8s cluster running. In this section, we provide examples for the tools **KinD** and **Minikube**.


### With KinD

1. Create kind config file:
```
    cat > kind-config.yaml <<EOF
    # three node (two workers) cluster config
    kind: Cluster
    apiVersion: kind.x-k8s.io/v1alpha4
    nodes:
    - role: control-plane
    - role: worker
    - role: worker
    EOF
```
 2 . Create cluster:
```
    kind create cluster --name <cluster_name> --config kind-config.yaml
```

To delete the cluster:
```
    kind delete cluster --name <cluster_name>
```



### With minikube


Set up a cluster with (x) number of nodes under the profile codeco:
```
    minikube start --nodes x -p <cluster-name> 
```

To delete the cluster:
``` 
  minikube delete -- all 

 ``` 

## 4.2 Get data from your cluster

PDLC-CA will use data collected by the CODECO components NetMA (network metrics); MDM (data workflow metrics); ACM (application and user metrics), by running the CODECO synthetic data generator on your cluster](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/data-generators-and-datasets/synthetic-data-generator) to be able to retrive metrics that is used as inputs to the PDLC-CA component.


## 4.3 Deploy PDLC-CA in the cluster

Deploy the CA controller and its relevant deployment files, as part of the PDLC component deployment found [here](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/privacy-preserving-decentralised-learning-and-context-awareness-pdlc/pdlc-deployment) , or as part of the whole CODECO framework to be able to use metrics provided by the other components (e.g ACM, NetMA and MDM) [here](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/acm). 

After installing PDLC-CA as part of the CODECO framework, you can view the output CSV file containing performance profile costs for nodes within the cluster by following these steps:

1. Get the Pod Name: Use the kubectl get pods command to retrieve the name of the PDLC-CA pod:


 ``` 
POD_NAME=$(kubectl get pods -n he-codeco-pdlc -l app=pdlc-ca -o jsonpath="{.items[0].metadata.name}") 

 ``` 
 
2. Execute the command within the pod: Use the kubectl exec command execute a shell command inside the retrieved pod. This command changes the directory to /data/RL and displays the contents of metrics.csv:

 ``` 
kubectl exec -it $POD_NAME -- sh -c "cd /data/RL && cat metrics.csv"

 ``` 



## 4.4 Changes to the code: 
If you would like to change anything in the code of PDLC-CA, then you are required to make a new image to run your new changes. You can build a new image and push it to docker hub or you can do this locally with KinD or Minikube with the steps below: 

### Build new image directly (locally) in minikube:


1 . Build the docker file in minikube:
``` 
    minikube -p <cluster_name> image build -t <IMAGE_NAME:VERSION> -f Dockerfile .
``` 
You can confirm its existence with the following command:
``` 
    minikube -p <cluster_name> image ls     
 ``` 
2 . Change the image name in the ca_controller/ca_controller_deployment.yaml file to : IMAGE_NAME:VERSION


**Second method: Using local private registry:**

1 . enable the registry addon:

```
    minikube addons enable registry
 ```

you can confirm it using :
```
    kubectl get service --namespace kube-system

 ```
2 . Use kubectl port-forward to map your local workstation to the minikube vm
```
    kubectl port-forward --namespace kube-system service/registry 5000:80
```
3 . run socat there to redirect traffic going to the docker vm’s port 5000 to port 5000 on your host workstation:
```
    docker run --rm -it --network=host alpine ash -c "apk add socat && socat TCP-LISTEN:5000,reuseaddr,fork TCP:host.docker.internal:5000"
```
4 . Once socat is running it’s possible to push images to the minikube registry from your local workstation:
```
    docker tag <IMAGE_ID> localhost:5000/ca_controller
    docker push localhost:5000/ca_controller
```
5. After the image is pushed, refer to it by localhost:5000/ca_controller in ca_controller_deployment.yaml file

for more info check : https://minikube.sigs.k8s.io/docs/handbook/registry/

Now you can apply the K8s deployment files just like in the first steps to retrieve CA CRD. (refer to 4.3)

### Build new image directly (locally) in KinD:

1. Build image using docker:

``` 
 docker build --no-cache -t my-custom-image -f Dockerfile .
``` 

2. Load Image into the nodes containers of kind:

``` 
 kind load docker-image my-custom-image --name <cluster_name>
``` 

Now you can apply the K8s deployment files to retrieve the nodes costs. (refer to 4.3)


--------------------------------------------------------------------------


# 6. Status and TODOs

- This code is part of the Codeco Framework and is intended to be used with all its components. For standalone usage as a cost generator, along with a synthetic data generator to probe metrics involved in cost equations, please refer to the other branch: PDLC-CA-Standalone. This will allow for adjustments and independent operation outside the full framework, where probed metrics can be gathered by the data generator and the cosst can be estimeated by the code in the PDLC-CA-Standalone.


# 7. License
Code and documentation Copyright 2024 HE-CODECO, author fortiss GmbH, under Apache 2.0.
Documentation is licensed under CC.

# 8. Credits and Acknowledgements
PDLC-CA is a sub-component of the CODECO PDLC. This work is under development in the context of the HE CODECO project which is funded by the European Commission under Grant Number 101092696.

Team:
- Dalal Ali (ali at fortiss dot org)
- Rute C. Sofia
