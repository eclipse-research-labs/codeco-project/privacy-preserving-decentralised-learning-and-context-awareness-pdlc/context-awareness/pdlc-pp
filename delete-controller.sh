# Copyright (c) 2023 fortiss GmbH
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Dalal Ali - Author

# chmod -R 777 .
# ./delete-controller.sh

# Delete the all deployments
kubectl delete -f ca_controller/ca-controller-deployment.yaml
kubectl delete -f ca_controller/ca-role-binding.yaml
kubectl delete -f ca_controller/ca-role.yaml
kubectl delete -f ca_controller/ca-crd.yaml

