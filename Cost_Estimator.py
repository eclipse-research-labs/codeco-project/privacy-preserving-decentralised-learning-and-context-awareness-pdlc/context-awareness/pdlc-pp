# Copyright (c) 2023 fortiss GmbH
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Dalal Ali - Author


## ca-component.py has as input the CODECO Application Model performance_profile and generates as output an aggregated value 
## for that profile, for each node, based on CODECO NetMA, MDM, ACM parameters being monitored.

import time
import os
import csv
from datetime import datetime
from Extractor import get_metrics




class Node:
    def __init__(self, name,node_cpu, node_mem, node_energy,node_net_energy, node_net_failure, node_failure,congestion,node_degree,ebw,ibw):
      
     # CA Parameters of the Node:
        self.name = name

     ## Compute Parameters:   
        self.node_cpu = node_cpu
        self.node_mem = node_mem
        self.node_energy = node_energy
        self.node_net_energy = node_net_energy # aggregated for all links of a node

     ## Network Parameters: 
     ## @TODO obtain all parameters from NetMA CRDs
        self.node_net_failure = node_net_failure # aggregated for all links of a node
        self.node_failure= node_failure
        self.congestion = congestion
        self.node_degree = node_degree
        self.ebw = ebw 
        self.ibw = abs (ibw) # dummy value might be negative
   
   



class ContextAwareness:
    def __init__(self, nodes):
        self.nodes = nodes

 #currently an example for 2 feasible performance_profiles
 # @TODO: read directly the CODECO Application Model to get the target performance profiles provided by the user DEV
    def compute_costs(self, performance_profile):

        # Normalize the attributes to be between 0 and 1
        # @TODO: has to be revised, as the max values will vary from round to round
        
        node_energy_max           = max([node.node_energy for node in self.nodes])
        node_net_energy_max       = max([node.node_net_energy for node in self.nodes])
        ebw_max              = max([node.ebw for node in self.nodes])
        ibw_max              = max([node.ibw for node in self.nodes])
        node_failure_max     = max([node.node_failure for node in self.nodes]) 
        node_net_failure_max = max([node.node_net_failure for node in self.nodes]) 
        node_degree_max = max([node.node_degree for node in self.nodes])

        # To avoid dividing by null values later on
        if node_energy_max == 0: node_energy_max =0.0000000000000000001
        if node_net_energy_max == 0: node_net_energy_max =0.0000000000000000001
        if ebw_max == 0: ebw_max =0.0000000000000000001
        if ibw_max == 0: ibw_max =0.0000000000000000001
        if node_failure_max == 0: node_failure_max =0.0000000000000000001
        if node_net_failure_max == 0: node_net_failure_max =0.0000000000000000001
        if node_degree_max == 0: node_degree_max =0.0000000000000000001
    
            

        # On a first step, metric values are normalized:
        norm_nodes = []
        norm_node= {}

        for node in self.nodes:
          # @TODO: add all parameters monitored  in CODECO
            
            norm_node ["node_name"]= node.name
            norm_node["node_energy"]= node.node_energy / node_energy_max
            norm_node["node_net_energy"]= node.node_net_energy/ node_net_energy_max
            norm_node["ebw"]= node.ebw / ebw_max
            norm_node["ibw"]= node.ibw / ibw_max
            norm_node["node_net_failure"]= node.node_net_failure / node_net_failure_max
            norm_node["node_failure"]= node.node_failure / node_failure_max
            norm_node["node_degree"]=node.node_degree / node_degree_max


            if norm_node["node_energy"] == 0: norm_node["node_energy"] =0.0000000000000000001
            if norm_node["ebw"] == 0: norm_node["ebw"] =0.0000000000000000001
            if  norm_node["node_failure"] == 0: norm_node["ibw"] =0.0000000000000000001
            if norm_node["node_net_failure"]== 0: norm_node["node_net_failure"] =0.0000000000000000001
            if norm_node["node_failure"] == 0: norm_node["node_failure"]=0.0000000000000000001
            if norm_node["node_degree"] == 0: norm_node["node_degree"] =0.0000000000000000001
            norm_nodes.append(norm_node)
            norm_node= {} #reset

        #print("Norm Nodes Metrics")
        #print (norm_nodes)


        # define attributes weights:
        #For performance_profile == "Greenness": # as all weights are equal, we do not consider this in this stage
        energy_weight = 1
        bandwidth_weight = 1

        #For performance_profile == "Resilience": # as all weights are equal, we do not consider this in this stage
        bandwidth_weight = 1
        Failure_weight = 1
        Congestion_weight= 1
        Degree_weight =1

     #Profiles costs computation: The lower the cost, the better alignement with the desired two profiles
        
        #1. Example for Greenness cost: The purpose is to select nodes that can contribute to achieve an infrastructure (for an application group) that has the least energy expenditure with lowest possible costs.       
        ## Greenness: greeness_cost(n)= node_energy(n)* node_net_energy(n), where n is a suitable node and available_bandwidth(n) is the sum of available bandwidth on egress links.
        ## Rationale: the lower the node energy expenditure and its associated links ; the lower is the node greenness cost.

        Costs_G = [] 
        for node in norm_nodes:
            node_greenness_cost = (node["node_energy"] * node["node_net_energy"])
            Costs_G.append(node_greenness_cost) #list of nodes costs

   

        #2. Example for resilience cost: Focuses on the selection of nodes that will maximize the resilience of the infrastructure with lowest possible costs.
        ## Resilience: resilience_cost(n)=node_net_failure*sum_link_failure*1/node_degree
        ## Rationale: the lower the number of failures of a node and it associated links; the higher the node_degree; the lower is the resilience costs of the node
        Costs_R = []
        for node in norm_nodes:
            cost = (node["node_net_failure"] * node["node_failure"]) / (node['node_degree']) 
            Costs_R.append (cost) #list of nodes costs



  
        # Cost Linear normalization (“Max-Min”) and scaling to be between 0 and 100:
        Greenness_Sort= []
        Resilience_Sort=[]
        ProfilesCosts=[]
        row = {}
        for i, node in enumerate (norm_nodes) :
               
         ## Greenness Costs Norm:
            if max (Costs_G) ==  min (Costs_G): 
                 NormCost = 100 * (Costs_G[i])
            else: 
                 NormCost = 100 * ((Costs_G[i]- min (Costs_G)) / (  max (Costs_G)- min (Costs_G)) ) 
            row["node_name"]= node["node_name"]
            row["Greennness_Cost"]= NormCost
            Greenness_Sort.append((node["node_name"],NormCost))  

         ## Resilience Costs Norm:
            if max (Costs_R) ==  min (Costs_R): 
                 NormCost = 100 * (Costs_R[i])
            else: 
                 NormCost = 100 * ((Costs_R[i]- min (Costs_R)) / (max (Costs_R)- min (Costs_R))) 
            
            row["Resilience_Cost"]= NormCost
            Resilience_Sort.append((node["node_name"], NormCost)) 
            ProfilesCosts.append(row)
            row={}   
      

     
        return ProfilesCosts




# Set the interval for clearing the file content (in seconds)
clear_interval = 3600 # 1 hour in secondsKubernetes
next_clear_time = time.time() + clear_interval

# Ensure directory exists
output_dir = '/data/RL/'
os.makedirs(output_dir, exist_ok=True)
output_csv = '/data/RL/metrics.csv'

# Check if the file exists
file_exists = os.path.isfile(output_csv)

# If the file doesn't exist, create it and write the header
if not file_exists:
    with open(output_csv, mode='w', newline='') as csv_file:
        fieldnames = ['timestamp', 'node_name', 'greenness_cost', 'resilience_cost']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        print(f"Created CSV file: {output_csv}")


while True: 
     cluster = []
     metrics= get_metrics()
  
     for row in metrics:
         # Node (name,node_cpu, node_mem, node_energy,node_net_energy, node_net_failure, node_failure,congestion,node_degree,ebw,ibw):
         # metrics that are not available has zero is allotted instead. 
         cluster.append(Node(
                 name=row['node_name'],
                 node_cpu=float(row['node_cpu_usage']),
                 node_mem=float(row['node_memory_values']),
                 node_energy=float(row['node_energy_values']),
                 node_net_energy=float(0),  # dummy value, you can replace it with the actual column name
                 node_net_failure=float(0),  
                 node_failure=float(0),
                 congestion=float(0),  
                 node_degree=float(0),
                 ebw=float(0),
                 ibw=float(0)
             ))
     

 
     CA = ContextAwareness(cluster)
 
     # Profile specifed in application request:
     # current example is static; @TODO read selected profile(s) from the ApplicationModel file
     Profile = "Greenness"
     ranking = CA.compute_costs(Profile)
 
     print(' \nProfiles Costs For The Cluster: \n')
     print(' {:<13} {:<25} {:<20}'.format('Name', 'Greennness_Cost', 'Resilience_Cost'))
 
     for i, node in enumerate(ranking):
         print(' {:<13} {:<25} {:<20}'.format(node["node_name"], node["Greennness_Cost"], node["Resilience_Cost"]))
 
            
 
     # CA's data generation:
     cost_list = {}
     score_list_g = {}
     score_list_r = {}
 
     # greennes_cost:
     for node in ranking:
        score_list_g[node["node_name"]] = node["Greennness_Cost"]
     # resilience_cost:
     for node in ranking:
       score_list_r[node["node_name"]] = node["Resilience_Cost"]
 
     def fetch_CA(node_name):
       return {
          "greenness_cost": score_list_g[node_name],
          "resilience_cost": score_list_r[node_name]}
 
     # Extract list of cluster nodes: 
     nodes = [entry['node_name'] for entry in metrics]
 
     ################################### Store costs in output CSV file:#####################################################
 

      # Check if the file exists to determine whether to write the header
     write_header = not os.path.isfile(output_csv)

      # Check if it's time to clear the file content
     if time.time() >= next_clear_time:
         # Open the file in write mode to clear its content
         with open(output_csv, mode='w', newline='') as clear_file:
             clear_file.truncate(0)  # Clear the file content
             print("CSV file content cleared:", output_csv)               
  
         # Update the next clear time
         next_clear_time = time.time() + clear_interval
    
     with open(output_csv, mode='a', newline='') as csv_file:
       fieldnames = ['timestamp', 'node_name', 'greenness_cost', 'resilience_cost']
       writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
 

       # Write CSV header only if the file is created for the first time
       if csv_file.tell() == 0:
         writer.writeheader()

 
       for node in nodes:
        answer = fetch_CA(node)
 
        # Write data to CSV file with timestamp
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        writer.writerow({
            'timestamp': timestamp,
            'node_name': node,
            'greenness_cost': answer["greenness_cost"],
            'resilience_cost': answer["resilience_cost"]
        })
 
       print("\nCSV file updated:", output_csv)
   
     
        
 
       time.sleep(1)
 

